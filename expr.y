%code requires {
  #include "ast.h"
}

%{
#include <stdio.h>
#include "utils.h"

int yylex();
extern int yylineno;

void yyerror(const char* msg) {
  printf("Line %d: %s\n",yylineno,msg);
}
int print_value;
#define YYERROR_VERBOSE 1
%}

%union {
  Statement *statement_t;
  Expr *expr_t;
  int int_t;
  char *char_t;
}


%type <statement_t> stmt assign_st print_st stmts optional_option embededd_statement
%type <statement_t> if_st braket_block_stmts local_variable_declaration optional_else
%type <expr_t> expr term factor
%type <expr_t> conditional_expr equality_expr equality_expr_p rational_expr rational_expr_p
%type <int_t> print_option

%token OP_ADD OP_SUB OP_MUL OP_DIV TK_LEFT_PAR TK_RIGHT_PAR OP_COMMA
%token<int_t> TK_NUMBER
%token TK_EOF
%token TK_EOL
%token TK_ERROR
%token<char_t> TK_ID
%token OP_ASSIGN KW_PRINT KW_HEX KW_BIN KW_DEC
%token KW_IF KW_ELSE
%token OP_EQUAL OP_NOT_EQUAL OP_LESS_THAN OP_GREATER_THAN OP_LESS_OR_EQUAL OP_GREATER_OR_EQUAL
%token TK_OPEN_BRAKET TK_CLOSE_BRAKET

%%
input: eols_op stmts eols_op { $2->exec(); }
;
eols_op: eols
      |
;
eols: eols TK_EOL
    | TK_EOL
;
stmts : stmts eols stmt { $$ = $1; ((BlockStatement *)$$)->addStatement($3); }
      | stmt { $$ = new BlockStatement; ((BlockStatement *)$$)->addStatement($1); }
;
stmt: local_variable_declaration { $$= $1; }
    | embededd_statement { $$ = $1; }
;

local_variable_declaration: print_st { $$ = $1; }
                          | assign_st { $$ = $1; }
;
embededd_statement: if_st { $$ = $1; }
                  | braket_block_stmts { $$ = $1; }
;

if_st: KW_IF TK_LEFT_PAR conditional_expr TK_RIGHT_PAR eols local_variable_declaration TK_EOL optional_else { $$ = new IfStatement($3,$6, $8); }
     | KW_IF TK_LEFT_PAR conditional_expr TK_RIGHT_PAR eols embededd_statement TK_EOL optional_else { $$ = new IfStatement($3,$6,$8); }
;

optional_else: KW_ELSE eols stmt { $$ =  $3;}
             | { $$=NULL; }
;
braket_block_stmts: TK_OPEN_BRAKET eols_op stmts eols_op TK_CLOSE_BRAKET { $$ = $3; }
;

print_st: KW_PRINT expr optional_option { $$ = $3; }
;

optional_option: OP_COMMA print_option { $$ = new PrintStatement($<expr_t>0, $2); }
               | {$$ = new PrintStatement($<expr_t>0);}
;

print_option: KW_DEC { $$ = DEC; }
            | KW_HEX { $$ = HEX; }
            | KW_BIN { $$ = BIN; }
;
assign_st: TK_ID OP_ASSIGN expr { $$ = new AssignStatement($1, $3); }
;
conditional_expr: equality_expr { $$ = $1; }
;
equality_expr: rational_expr equality_expr_p { $$ = $2; }
;

equality_expr_p: OP_EQUAL expr { $$ = new EqualExpr($<expr_t>0, $2); }
               | OP_NOT_EQUAL expr { $$ = new NotEqualExpr($<expr_t>0, $2); }
               | { $$ = $<expr_t>0; }
;
rational_expr: expr rational_expr_p { $$ = $2; }
;

rational_expr_p: OP_GREATER_THAN expr { $$ = new GreaExpr($<expr_t>0, $2); }
               | OP_LESS_THAN expr { $$ = new LessExpr($<expr_t>0, $2); }
               | OP_GREATER_OR_EQUAL expr { $$ = new GreaEqExpr($<expr_t>0, $2); }
               | OP_LESS_OR_EQUAL expr { $$ = new LessEqExpr($<expr_t>0, $2); }
               | {$$ = $<expr_t>0;}
;

expr: expr OP_ADD term { $$ = new AddExpr($1, $3); }
    | expr OP_SUB term { $$ = new SubExpr($1, $3); }
    | term { $$ = $1; }
;

term: term OP_MUL factor { $$ = new MulExpr($1, $3); }
    | term OP_DIV factor { $$ = new DivExpr($1, $3); }
    | factor { $$ = $1; }
;

factor: TK_NUMBER { $$ = new NumberExpr($1); }
    | TK_LEFT_PAR expr TK_RIGHT_PAR { $$ = $2; }
    | TK_ID { $$ = new VarExpr($1); }
;
